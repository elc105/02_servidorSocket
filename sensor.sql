/*
 Navicat Premium Data Transfer

 Source Server         : LOCAL(MySQL)
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : sensor

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 19/06/2022 11:55:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for reglas
-- ----------------------------
DROP TABLE IF EXISTS `reglas`;
CREATE TABLE `reglas`  (
  `ReglaID` int(5) NOT NULL AUTO_INCREMENT,
  `MinT` int(3) NULL DEFAULT NULL,
  `MaxT` int(3) NULL DEFAULT NULL,
  `MinH` int(3) NULL DEFAULT NULL,
  `MaxH` int(3) NULL DEFAULT NULL,
  `typeRule` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `CommandRule` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `MessageRule` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `_CreatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `State` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'A',
  `email` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`ReglaID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of reglas
-- ----------------------------
INSERT INTO `reglas` VALUES (1, 40, 70, 10, 20, 'T', 'between', 'la temperatura está extremadamente alta', '2022-05-19 12:06:10.721015', 'A', 'diancris@gmail.com,omicronanime@gmail.com,sensorelc105@gmail.com');
INSERT INTO `reglas` VALUES (2, 11, 20, 60, 70, 'T', 'between', 'La temperatura esta muy baja', '2022-05-19 12:07:25.795289', 'A', 'diancris@gmail.com');
INSERT INTO `reglas` VALUES (3, 25, 39, 60, 80, 'T', 'May', 'La temperatura es muy alta', '2022-05-19 12:08:28.434421', 'A', 'omicronanime@gmail.com');

-- ----------------------------
-- Table structure for sensores
-- ----------------------------
DROP TABLE IF EXISTS `sensores`;
CREATE TABLE `sensores`  (
  `DispositivoID` int(10) UNSIGNED NOT NULL,
  `_CreatetAT` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `IpAddress` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Dispositivo` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`DispositivoID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sensores
-- ----------------------------

-- ----------------------------
-- Table structure for temperatura
-- ----------------------------
DROP TABLE IF EXISTS `temperatura`;
CREATE TABLE `temperatura`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `Temperatura` int(4) NOT NULL,
  `humedad` int(4) NOT NULL,
  `FechaRegistro` datetime(6) NOT NULL,
  `ClienteID` int(4) NOT NULL,
  `_CreatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of temperatura
-- ----------------------------
INSERT INTO `temperatura` VALUES (1, 50, 60, '2022-05-19 08:32:39.780341', 102145, '2022-05-19 08:33:02.315435');
INSERT INTO `temperatura` VALUES (2, 50, 60, '2022-05-19 11:07:46.825422', 102145, '2022-05-19 11:07:57.574201');
INSERT INTO `temperatura` VALUES (3, 50, 60, '2022-05-19 10:25:00.000000', 102145, '2022-05-19 11:39:18.362191');
INSERT INTO `temperatura` VALUES (4, 50, 60, '2022-05-19 10:25:00.000000', 102145, '2022-05-19 15:47:56.821930');
INSERT INTO `temperatura` VALUES (5, 25, 60, '2022-05-19 10:25:00.000000', 102145, '2022-05-19 15:54:16.665669');
INSERT INTO `temperatura` VALUES (6, 30, 60, '2022-05-19 10:25:00.000000', 102145, '2022-05-19 15:56:47.196741');
INSERT INTO `temperatura` VALUES (7, 30, 60, '2022-05-19 10:25:00.000000', 102145, '2022-05-19 16:11:42.923770');
INSERT INTO `temperatura` VALUES (8, 31, 45, '2022-05-19 16:25:00.000000', 102145, '2022-05-19 16:24:10.210410');
INSERT INTO `temperatura` VALUES (9, 31, 45, '2022-05-19 16:25:00.000000', 102145, '2022-05-19 16:38:01.358774');
INSERT INTO `temperatura` VALUES (10, 12, 35, '2022-05-19 16:27:00.000000', 102145, '2022-05-19 16:40:19.832521');
INSERT INTO `temperatura` VALUES (11, 45, 15, '2022-05-19 16:28:00.000000', 102145, '2022-05-19 16:41:06.736193');
INSERT INTO `temperatura` VALUES (12, 19, 60, '2022-05-19 10:25:00.000000', 102145, '2022-05-19 16:43:13.507408');
INSERT INTO `temperatura` VALUES (13, 31, 45, '2022-05-19 16:25:00.000000', 102145, '2022-05-19 16:45:43.598930');
INSERT INTO `temperatura` VALUES (14, 45, 15, '2022-05-19 16:28:00.000000', 102145, '2022-05-19 17:10:54.450059');

-- ----------------------------
-- Procedure structure for spr_Listados
-- ----------------------------
DROP PROCEDURE IF EXISTS `spr_Listados`;
delimiter ;;
CREATE PROCEDURE `spr_Listados`(IN p_Id  int (4))
BEGIN	
     IF (p_Id <> 0) THEN
         SELECT * FROM temperatura WHERE id = p_Id;
     ELSE                                    
	       SELECT * FROM temperatura;
     END IF;         
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
