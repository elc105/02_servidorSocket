﻿using System;

namespace ServidorSocket
{
    interface ISocketListener
    {
        event EventHandler OnClientConnected;
        event EventHandler OnClientDisconnected;
        event EventHandler OnListenerDisconnected;
    }

    interface IMessageListener : IDisposable
    {
        event EventHandler OnReceivedData;
        event EventHandler OnSendedData;
    }


    
}
