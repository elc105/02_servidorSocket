﻿using System;
using System.Collections.Generic;

namespace ServidorSocket.Data
{
    public partial class Temperatura
    {
        public int Id { get; set; }
        public float Temperatura1 { get; set; }
        public float Humedad { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string ClienteId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
