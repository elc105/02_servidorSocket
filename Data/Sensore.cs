﻿using System;
using System.Collections.Generic;

namespace ServidorSocket.Data
{
    public partial class Sensore
    {
        public uint DispositivoId { get; set; }
        public DateTime CreatetAt { get; set; }
        public string IpAddress { get; set; }
        public string Estado { get; set; }
        public string Dispositivo { get; set; }
        public float? UltTemp { get; set; }
        public float? UltHum { get; set; }
        public DateTime? UltConexion { get; set; }
    }
}
