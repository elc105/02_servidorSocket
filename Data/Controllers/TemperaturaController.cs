﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServidorSocket.Data.Controllers
{
    public class TemperaturaController
    {
        private readonly sensorContext db;

        public TemperaturaController()
        {
            this.db = new sensorContext();
        }
        public List<Temperatura> getTemperatura()
        {
            return db.Temperaturas.ToList();
        }

        public bool GuardarTemperatura(string data)
        {
            var r = GetTemperaturaParser(data);

            if (r == null) return false;
            try
            {
                db.Add(r);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }
         public bool GuardarTemperatura(Temperatura data)
        {
            if (data == null) return false;
            try
            {
                db.Add(data);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        private Temperatura GetTemperaturaParser(string s)
        {
            var data = s.Split("\r\n");

            if (data is Array)
            {
                var b = new Temperatura()
                {
                    Temperatura1 = float.Parse(data[1]),
                    Humedad = float.Parse(data[2]),
                    FechaRegistro = DateTime.Parse(data[3]),
                    ClienteId = data[0],
                };
                return b;
            }
            return null;
            

            
        }


        private static Array GetItemArray(string data)
        {
            return null;
        }
    }
}
