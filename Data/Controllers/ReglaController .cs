﻿using ServidorSocket.helper;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace ServidorSocket.Data.Controllers
{
    public class ReglaController
    {
        private readonly sensorContext db;

        public ReglaController()
        {
            this.db = new sensorContext();
        }

        public bool GuardarRegla(object data)
        {
            if (data == null) return false;

            var r = new Regla();
            try
            {
                db.Add(r);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }
        
        public Regla ObtenerRegla(float Temperatura1)
        { 
           // var r = Helper.GetParser2(s);

            var m =  db.Reglas.Where(x => Temperatura1>=x.MinT  && Temperatura1<=x.MaxT && x.TypeRule =="T").FirstOrDefault();
            return m;
        }
        /*
        public Regla ObtenerRegla(float Humedad)
        {
            var m = db.Reglas.Where(x => Humedad >= x.MinT && temp <= x.MaxT && x.TypeRule == "T").FirstOrDefault();
            return m;
        }*/

        /*
        public Regla ObtenerRegla(String s, Parametro param = null)
        {
            var r = GetParser(s);
            Expression<Func<Regla,bool>> where = c => c.State != null;

            if (param == null)
                where = where.And(x => x.MinT >= r.Temperatura1 && x.MaxT <= r.Temperatura1 && x.TypeRule == "T");
            else
            {
                where = where.And(x => x.TypeRule == param.Trule);
                switch (param.Trule)
                {
                    case "T":
                        {
                            if (param.cmdRule == "Between")
                                where = where.And(x => r.Temperatura1 >= x.MinT && r.Temperatura1 <= MaxT);

                            if (param.cmdRule == "May")
                                where = where.And(x => r.Temperatura1 > x.MinT);

                            break;
                        }
                    case "H":
                        {
                            if (param.cmdRule == "Between")
                                where = where.And(x => r.Humedad >= x.MinH && r.Humedad <= MaxH);

                            if (param.cmdRule == "May")
                                where = where.And(x => r.Humedad > x.MinH);
                            break;
                        }


                }

            }
            var res = db.Reglas.Where(where).FirstOrDefault();



                // var m = db.Reglas.Where(x => x.MinT >= r.Temperatura1 && x.MaxT <= r.Temperatura1 && x.TypeRule == "T").FirstOrDefault();
                return res;
        }

        */
       
    }

    public sealed class Parametro
    {
        public string Trule {get; set;}
        public string cmdRule { get; set; }
    }
}
