﻿using ServidorSocket.helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServidorSocket.Data.Controllers
{
    public class SensoresController
    {
        private readonly sensorContext db;

        public SensoresController()
        {
            this.db = new sensorContext();
        }
        public List<Sensore> getListaSensores()
        {
            return db.Sensores.ToList();
        }

        public bool GuardarSensor(string data)
        {
            var r = Helper.GetTemperaturaParser(data);

            if (r == null) return false;
            try
            {
                db.Add(r);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }
        public bool RegistrarConectado(Sensore data)
        {
            var ss = db.Sensores.Where(x => x.Dispositivo == data.Dispositivo).FirstOrDefault();

            if (ss == null) // si es nuevo, registra en la BD
            {
                try
                {
                    db.Add(data);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
            }
            else // actualiza el estado del dispositivo
            {
                try
                {
                    ss.Estado = "C";
                    db.SaveChanges();
                    return true;

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
            }
            
        }

        public bool ActualizarEstado(string SensorID, string estado)
        {
            var ss = db.Sensores.Where(x => x.Dispositivo == SensorID).First();

            if (ss == null) return true;
            
            try
            {
                ss.Estado = "D";
                db.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            

        }

        public bool iniciarServicio()
        {
            var ss = db.Sensores.ToList();

            if (ss == null) return true;

            try
            {
                foreach (var item in ss)
                {
                    item.Estado = "D";
                }
                db.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }


        }

        private static Array GetItemArray(string data)
        {
            return null;
        }
    }
}
