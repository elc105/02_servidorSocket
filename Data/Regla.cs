﻿using System;
using System.Collections.Generic;

namespace ServidorSocket.Data
{
    public partial class Regla
    {
        public int ReglaId { get; set; }
        public int? MinT { get; set; }
        public int? MaxT { get; set; }
        public int? MinH { get; set; }
        public int? MaxH { get; set; }
        public string TypeRule { get; set; }
        public string CommandRule { get; set; }
        public string MessageRule { get; set; }
        public DateTime CreatedAt { get; set; }
        public string State { get; set; }
        public string Email { get; set; }
    }
}
