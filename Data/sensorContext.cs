﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ServidorSocket.Data
{
    public partial class sensorContext : DbContext
    {
        public sensorContext()
        {
        }

        public sensorContext(DbContextOptions<sensorContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Regla> Reglas { get; set; }
        public virtual DbSet<Sensore> Sensores { get; set; }
        public virtual DbSet<Temperatura> Temperaturas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string ss = "";
            if (!optionsBuilder.IsConfigured)
            {
                ss = "server=" + ConfigurationManager.AppSettings.Get("Server")+";";
                ss+= "user="+ConfigurationManager.AppSettings.Get("Username") + ";";
                ss+= "password="+ConfigurationManager.AppSettings.Get("Password") + ";";
                ss += "database=" + ConfigurationManager.AppSettings.Get("BD");

                optionsBuilder.UseMySql(ss, Microsoft.EntityFrameworkCore.ServerVersion.Parse("5.6.16-mysql"));

                //optionsBuilder.UseMySql("server=localhost;user=root;password=123456;database=sensor", Microsoft.EntityFrameworkCore.ServerVersion.Parse("5.6.16-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("latin1_swedish_ci")
                .HasCharSet("latin1");

            modelBuilder.Entity<Regla>(entity =>
            {
                entity.ToTable("reglas");

                entity.Property(e => e.ReglaId)
                    .HasColumnType("int(5)")
                    .HasColumnName("ReglaID");

                entity.Property(e => e.CommandRule)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.CreatedAt)
                    .HasMaxLength(6)
                    .HasColumnName("_CreatedAt")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP(6)");

                entity.Property(e => e.Email)
                    .HasColumnType("text")
                    .HasColumnName("email");

                entity.Property(e => e.MaxH).HasColumnType("int(3)");

                entity.Property(e => e.MaxT).HasColumnType("int(3)");

                entity.Property(e => e.MessageRule).HasMaxLength(255);

                entity.Property(e => e.MinH).HasColumnType("int(3)");

                entity.Property(e => e.MinT).HasColumnType("int(3)");

                entity.Property(e => e.State)
                    .HasMaxLength(1)
                    .HasDefaultValueSql("'A'")
                    .IsFixedLength();

                entity.Property(e => e.TypeRule)
                    .IsRequired()
                    .HasMaxLength(1)
                    .HasColumnName("typeRule")
                    .IsFixedLength();
            });

            modelBuilder.Entity<Sensore>(entity =>
            {
                entity.HasKey(e => e.DispositivoId)
                    .HasName("PRIMARY");

                entity.ToTable("sensores");

                entity.Property(e => e.DispositivoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("DispositivoID");

                entity.Property(e => e.CreatetAt)
                    .HasMaxLength(6)
                    .HasColumnName("_CreatetAT")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP(6)");

                entity.Property(e => e.Dispositivo)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Estado)
                    .HasMaxLength(1)
                    .HasColumnName("estado")
                    .IsFixedLength();

                entity.Property(e => e.IpAddress).HasMaxLength(20);

                entity.Property(e => e.UltConexion).HasMaxLength(6);

                entity.Property(e => e.UltHum).HasColumnType("float(5,2)");

                entity.Property(e => e.UltTemp).HasColumnType("float(5,2)");
            });

            modelBuilder.Entity<Temperatura>(entity =>
            {
                entity.ToTable("temperatura");

                entity.Property(e => e.Id)
                    .HasColumnType("int(4)")
                    .HasColumnName("id");

                entity.Property(e => e.ClienteId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("ClienteID");

                entity.Property(e => e.CreatedAt)
                    .HasMaxLength(6)
                    .HasColumnName("_CreatedAt")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP(6)");

                entity.Property(e => e.FechaRegistro).HasMaxLength(6);

                entity.Property(e => e.Humedad)
                    .HasColumnType("float(5,2)")
                    .HasColumnName("humedad");

                entity.Property(e => e.Temperatura1)
                    .HasColumnType("float(5,2)")
                    .HasColumnName("Temperatura");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
