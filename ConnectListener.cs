﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace ServidorSocket
{
    class ConnectListener : BaseThread, ISocketListener

    {
        public Socket servidor;
        public int puerto;
        public string IpAddr;
        public int MaxConexiones = 100;
        public event EventHandler OnClientConnected;
        public event EventHandler OnClientDisconnected;
        public event EventHandler OnListenerDisconnected;
        public ConnectListener(int Puerto, string IP)
        { 
            puerto = Puerto;
            IpAddr = IP; ;
            IPEndPoint IPp = new IPEndPoint(IPAddress.Parse(IpAddr), puerto);
            servidor = new Socket(IPp.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            servidor.Bind(IPp);
            servidor.Listen(MaxConexiones);
        }
        
        public override void RunThread()
        {
            Listenning();
        }

        public void Listenning()
        {
            Socket cli;
            try
            {
                while (true)
                {
                    cli = servidor.Accept();
                    Connect(cli, EventArgs.Empty);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("serv", ex.ToString());
                Disconect(this, EventArgs.Empty);
            }
        }

        private void Connect(Socket cli, EventArgs e)
        {
            OnClientConnected?.Invoke(cli, e);
        }

        private void Disconect(object cli, EventArgs e)
        {
            //OnClientDisconnected?.Invoke(cli, e);
            OnListenerDisconnected?.Invoke(cli, EventArgs.Empty);

        }
    }

    

    
    
}
