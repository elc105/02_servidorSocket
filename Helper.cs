﻿using ServidorSocket.Data;
using System;
using System.Collections.Generic;


namespace ServidorSocket.helper
{
    public class Helper 
    {
        public static Temperatura GetTemperaturaParser(string s)
        {
            var data = s.Split("\r\n");

            if (data is Array)
            {
                var b = new Temperatura()
                {
                    Temperatura1 = float.Parse(data[1]),
                    Humedad = float.Parse(data[2]),
                    //FechaRegistro = DateTime.Parse(data[3]),
                    FechaRegistro = DateTime.Now,
                    ClienteId = data[0],
                };
                return b;
            }
            return null;
        }
        public static List<Temperatura> GetParser(string s)
        {
            var data = s.Split("\r\n");

            if (data is Array)
            {
                List<Temperatura> lista = new List<Temperatura>();

                foreach (var kdt in data)
                {
                    var kdata = Parametros(kdt);

                    if (kdata != null)
                    {
                        lista.Add(kdata);
                    }
                }
                return lista;

            }
            else return null;

        }

        public static Temperatura Parametros(String data)
        {
            if (data == "") return null; // no tiene parametros

            var par = data.Split("|");

            if (par.Length != 4) return null;   // no tiene la cantidad de paràmetros correcto 

            //string[] value = new string[4];

            Temperatura value = new Temperatura();
            //value.Temperatura1 = Int32.Parse(par[0].Split("=")[1].Trim());
            var aux = par[1].IndexOf("=");
            string dataX = par[1].Substring(aux + 1);
            value.Temperatura1 = float.Parse(dataX);
            value.Humedad = float.Parse(par[2].Substring(par[2].IndexOf("=") + 1));
            //value.FechaRegistro = Convert.ToDateTime(float.Parse(par[3].Substring(par[3].IndexOf("=") + 1)));
            //value.FechaRegistro = DateTime.FromOADate( float.Parse(par[3].Substring(par[3].IndexOf("=") + 1)));
            value.FechaRegistro = DateTime.Now;
            value.ClienteId = par[0].Substring(par[0].IndexOf("=") + 1);

            return value;
        }

        public static Temperatura GetParser2(string s)
        {
            var data = s.Split("|");

            var b = new Temperatura()
            {
                Temperatura1 = float.Parse(data[0]),
                Humedad = float.Parse(data[1]),
                //FechaRegistro = DateTime.Parse(data[2]),
                FechaRegistro = DateTime.Now,
                ClienteId = data[3],
            };

            return b;
        }
    }
}
