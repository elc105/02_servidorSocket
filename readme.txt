para volcar laBD a entity framework
dotnet ef dbcontext scaffold "Server=localhost;User=root;Password=123456;Database=sensor" "Pomelo.EntityFrameworkCore.MySql"

especificando un directorio dentro del ámbito del proyecto
dotnet ef DbContext scaffold "Server=localhost;User=root;Password=123456;Database=sensor" "Pomelo.EntityFrameworkCore.MySql" --output-dir Data --force

cuando hay algun error de compilación
dotnet build

para instalar el dotnet ef
dotnet tool install --global dotnet-ef