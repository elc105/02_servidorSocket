﻿using EmailService;
using FireBaseNotificacion;
using Newtonsoft.Json;
using ServidorSocket.Data;
using ServidorSocket.Data.Controllers;
using ServidorSocket.helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using static FireBaseNotificacion.PushNotification;

namespace ServidorSocket
{
    class Monitor
    {
        private static TemperaturaController temp = new TemperaturaController();
        public static SensoresController Sensor = new SensoresController();
        private static ReglaController regla = new ReglaController();
        private static GestorCorreo email = new GestorCorreo();
        private static Monitor monitor;

        public static void Main(string[] args)
        {
            monitor = new Monitor();
            
            String IP = ConfigurationManager.AppSettings.Get("IP");
            int Port = int.Parse(ConfigurationManager.AppSettings.Get("Puerto"));

            Servidor servidorSocket = new Servidor(IP, Port);
            servidorSocket.SendMessageToMonitor += LeerMensaje;
            servidorSocket.ConnectedSocketMonitor += SocketConectado;
            servidorSocket.DisconectedSocketMonitor += SocketDesconectado;
            servidorSocket.InitServidor();
            Sensor.iniciarServicio();
            Console.Read();
        }

        private static void LeerMensaje(object sender, EventArgs e)
        {
            string msg = (String)sender;
            var data = Helper.GetParser(msg.ToString());

            if (data == null) return;
            foreach(var t in data)
                monitor.RealizarTarea(t);
        }

        private static void SocketConectado(object sender, EventArgs e)
        {
            string msg = (String)sender;
            var data = Helper.Parametros(msg.ToString());

            if (data == null) return;
            DateTime datet = new DateTime();

            var res = Sensor.RegistrarConectado(new Sensore
            {
                Estado = "C",
                Dispositivo = data.ClienteId,
                UltTemp = data.Temperatura1,
                UltHum = data.Humedad,
                UltConexion = DateTime.Now

        }); 

            if (res == true)
                NotificacionPush();

        }

        private static void NotificacionPush()
        {
            string serverKey = "AAAATRJLcGk:APA91bEdzS3uUc-Yky76fcVs8pPzzaMOcZDU20AWbnavHJig8vGzMKLjk4zBc12Rmr_t1J7XC5Oo-s40hIVszZzj_90KVkqk3j46H98JyhH1fQ-DNWqzoMEj7HQaeCYLHqaE9-SXA1zn";
            var notificationInputDto = new
            {
                to = "excapfYpQU6gHKH8WE9niS:APA91bHusHT8V5swF01yUKiboez92xsHybNIFsa7WJq3UsaW_Syq3_BguLorMq6BN7koM1JpShuaZiGA3WfHlZubH-31hWUpZrfLwoskq5uTjxBcOfkSDGY66nO9qKoFzVnjQcToCD36",
                notification = new
                {
                    body = "Body of your Notification",
                    title = "Title of your Notification ",
                    icon = "",
                    type = ""
                },
                data = new
                {
                    key1 = "value1",
                    key2 = "value2"
                }
            };
            try
            {
                var result = "";
                var webAddr = "https://fcm.googleapis.com/fcm/send";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add("Authorization:key=" + serverKey);
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(JsonConvert.SerializeObject(notificationInputDto));
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        private static void SocketDesconectado(object sender, EventArgs e)
        {
            string idsensor = (String)sender;

            Sensor.ActualizarEstado(idsensor, "D");
            NotificacionPush();
        }
        public async Task RealizarTarea(Temperatura t)
        {
            temp.GuardarTemperatura(t);
            var oRegla = regla.ObtenerRegla(t.Temperatura1);

            if (oRegla != null)
                email.EnviarCorreoAsync(new MailMessage("sensorelc105@gmail.com", oRegla.Email, "Mensaje Monitor Sensor temperatura", oRegla.MessageRule + ":" + t.Temperatura1));
            return;
        }
        /*
        public List<Temperatura> GetParser(string s)
        {
            var data = s.Split("\r\n");

            if (data is Array)
            {
                List<Temperatura> lista = new List<Temperatura>();

                foreach (var kdt in data)
                {
                    var kdata = monitor.Parametros(kdt);

                    if (kdata != null)
                    {
                        /*
                        var b = new Temperatura();
                        b.Temperatura1 = Int32.Parse(kdata[0]);
                        b.Humedad = Int32.Parse(kdata[1]);
                        b.FechaRegistro = DateTime.Parse(kdata[2]);
                        b.ClienteId = Int32.Parse(kdata[3]);
                        */ /*

                        lista.Add(kdata);
                    }
                }
                return lista;

            }
            else return null;
            
        }*/ /*

        public Temperatura Parametros(String data)
        {
            if (data == "") return null; // no tiene parametros

            var par = data.Split("|");

            if (par.Length != 4) return null;   // no tiene la cantidad de paràmetros correcto 

            //string[] value = new string[4];

            Temperatura value = new Temperatura();
            //value.Temperatura1 = Int32.Parse(par[0].Split("=")[1].Trim());
            int s;
            value.Temperatura1 = Convert.ToInt32(par[0].Substring(par[0].IndexOf("=")+1));
            value.Humedad = Convert.ToInt32(par[0].Substring(par[0].IndexOf("=") + 1));
            value.FechaRegistro = Convert.ToDateTime(par[0].Substring(par[0].IndexOf("=") + 1));
            value.ClienteId = Convert.ToInt32(par[0].Substring(par[0].IndexOf("=") + 1)); 

            return value;
        }*/
    }
}
