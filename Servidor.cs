﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using ServidorSocket.helper;

namespace ServidorSocket
{
    class Servidor : ISocketListener, IMessageListener
    {

        //List<Conexion> ListaCli;
        public Dictionary<int, HiloCliente> ListaClientes;

        static ConnectListener listener;
        int puerto;
        string IP;
        int Max;

        public event EventHandler OnClientConnected;
        public event EventHandler OnClientDisconnected;
        public event EventHandler OnReceivedData;
        public event EventHandler OnSendedData;
        public event EventHandler OnListenerDisconnected;

        public event EventHandler SendMessageToMonitor;
        public event EventHandler ConnectedSocketMonitor;
        public event EventHandler DisconectedSocketMonitor;

        public Servidor(string Ip, int puerto)
        {
            this.puerto = puerto;
            this.IP = Ip;
            this.Max = 100;
        }

        public void InitServidor()
        {
            try
            {
                ListaClientes = new Dictionary<int, HiloCliente>();
                Console.WriteLine("servidor: Esperando conexiones");
                listener = new ConnectListener(puerto, IP);
                listener.OnClientConnected += OnClientConnect;
                listener.OnListenerDisconnected += OnlisteningDisconnect;
                listener.Start();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        public void OnclientDisconnect(object sender, EventArgs e)
        {
            if (sender is int)
            {
                var ClientID = (int)sender;
                if (ListaClientes.ContainsKey(ClientID))
                {
                    ListaClientes[ClientID].Cliente.Shutdown(SocketShutdown.Both);
                    ListaClientes[ClientID].Cliente.Close();
                    ListaClientes.Remove(ClientID);
                }
            }

            if (sender is Socket)
            {
                var cliente = (Socket)sender;
                if (ListaClientes.ContainsKey(cliente.Handle.ToInt32()))
                {
                    int id = cliente.Handle.ToInt32();
                    ListaClientes[id].Cliente.Shutdown(SocketShutdown.Both);
                    ListaClientes[id].Cliente.Close();
                    ListaClientes.Remove(id);
                }
            }
            
        }

        public void OnlisteningDisconnect(object sender, EventArgs e)
        {
            listener.servidor.Shutdown(SocketShutdown.Both);
            listener.servidor.Close();
            ListaClientes.Clear();
            Console.WriteLine("El servidor se cerró");
            Console.Out.Flush();
        }
        public void OnClientConnect(object sender, EventArgs e)
        {
            if (ListaClientes.Count() >= Max)
            {
                Console.WriteLine("el servidor ha sobrepasado el máximo nro de conexiones");
                Console.Out.Flush();
                return;
            }
            Socket aux = (Socket)sender;
            var s = new HiloCliente(aux);
            
            s.OnClientDisconnected += OnclientDisconnect;
            s.OnReceivedData += OnclientReceive;
            s.OnSendedData += OnclientSend;
            string mmm = "nuevo cliente conectado [" + s.ClientID.ToString() + "]\n";
            Console.WriteLine(mmm);
            Console.Out.Flush();
            s.Start();
            ListaClientes.Add(s.ClientID,s);
            
            //AsyncSendMsg(s.ClientID); // envia mensaje a todos los clientes que se ha conectado un nuevo cliente, menos al recien conectado

        }

        public void OnclientReceive(object sender, EventArgs e)
        {
            var s = (HiloCliente)sender;
            if (s.msg.IndexOf("<EOF>") > -1 || s.msg=="")
            {
                return;
            }


            if (s.ArdClient == "") // Si aun no se ha identificado el id de arduino, registra en la bd activo y lanza push notification
            {
                var ss = Helper.Parametros(s.msg);
                if (ListaClientes.ContainsKey(s.ClientID))
                {
                    ListaClientes[s.ClientID].ArdClient=ss.ClienteId;

                    //registra en la BD la nueva conecion de un socket
                    ConnectedSocketMonitor?.Invoke(s.msg, EventArgs.Empty);
                }
            }
            string mmm = "client ["+s.ClientID.ToString()+"]: "+s.msg+" \n";
            Console.WriteLine(mmm);
            Console.Out.Flush();
            SendMessageToMonitor?.Invoke(s.msg, EventArgs.Empty);
        }

        public void OnclientSend(object sender, EventArgs e)
        { 
            
        }
       
        public  async Task AsyncSendMsg(int v)
        {
            var s = ListaClientes.Where(f => f.Value.ClientID != v);
            string msg = "nuevo cliente Conectado:[" + v + "]\n";
            foreach (var d in s)
            {
                d.Value.Cliente.Send(Encoding.UTF8.GetBytes(msg), 0, msg.Length, SocketFlags.None);
            }
            return;
        }
        
        public async Task AsyncSendMsg(int v, string msg)
        {
            var s = ListaClientes.Where(f => f.Value.ClientID == v).FirstOrDefault();
            string message = "Serv dice: " + msg+"\n"; 
            
            s.Value.Cliente.Send(Encoding.UTF8.GetBytes(message), 0, message.Length, SocketFlags.None);
            
            return;
        }

        public async Task AsyncSendMsg(string msg, int v, bool flag = true)
        {
            string message = "Serv dice: El cliente: [" + v+"] se ha desconectado";

            foreach(var d in ListaClientes)
            {
                d.Value.Cliente.Send(Encoding.UTF8.GetBytes(msg), 0, msg.Length, SocketFlags.None);
            }
            return;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }

   
}
