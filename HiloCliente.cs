﻿using System;
using System.Net.Sockets;
using System.Net;
using System.Text;


namespace ServidorSocket
{
    class HiloCliente : BaseThread, IMessageListener, ISocketListener
    {
        
        public  string msg = null;
        private bool disposedValue;
        public int ClientID { get; set; }
        public string ArdClient { get; set; }
        public Socket Cliente { get; set; }
        public event EventHandler OnReceivedData;
        public event EventHandler OnSendedData;
        public event EventHandler OnClientConnected;
        public event EventHandler OnClientDisconnected;
        public event EventHandler OnListenerDisconnected;


        public HiloCliente(Socket hijo) { 
            Cliente = hijo;
            ClientID = hijo.Handle.ToInt32();
            ArdClient = "";
            //RunThread();
        }
        public override void RunThread()
        {
            Listenning();
        }
        public void RecibirData(Socket obj, string data)
        {
            //string s = (string)obj;
            msg = data;
            OnReceivedData?.Invoke(this, EventArgs.Empty);
        }

        public void EnviarData(Object obj, string data)
        {
            var handler = (Socket)obj;

            byte[] msg = Encoding.ASCII.GetBytes("Serv:"+data);
            OnSendedData?.Invoke(obj, EventArgs.Empty);
        }
        private void Desconectar(int id)
        { 
            OnClientDisconnected?.Invoke(Cliente, EventArgs.Empty);
        }

        public void Listenning()
        {
            
            byte[] bytes = new Byte[1024];
            try
            {
                while (true)
                {

                    int bytesRec = Cliente.Receive(bytes);

                    if (Cliente.Available>0 || Cliente.Connected)
                    {
                        msg = System.Text.Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        if (msg!="")
                        {
                            RecibirData(Cliente, msg);
                            if (ArdClient=="")
                                ActualizarCliente(msg);
                        }
                    }
                    else
                        Desconectar(ClientID);
                    //EnviarData(Cliente, msg);
                }
            }
            catch (Exception ex)
            {
                Desconectar(ClientID);
            }
        }

        public void ActualizarCliente(string data)
        { 
            
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
