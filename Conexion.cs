﻿using System;
using System.ComponentModel;
using System.Net.Sockets;


namespace ServidorSocket
{
    public class Conexion:EventArgs 
    {
        
        private int IdCliente;
        private Socket Cliente;
        
        public Conexion(Socket cli, IntPtr handle) {}
        public Conexion(Socket Cliente, int Id) 
        {
            Cliente = Cliente;
            IdCliente = Id;
        }
        public int GetIDCliente() {  return IdCliente; }
        public void SetIDCliente(int IDcliente) { this.IdCliente = IDcliente;}
        public Socket GetCliente() { return Cliente; }
        public void SetCliente(Socket Cliente) { this.Cliente = Cliente;}
    }
}
